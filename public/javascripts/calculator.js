function showPercent(row){
	var score = document.getElementById("scored"+(row+1)).value;
	var total_score = document.getElementById('total'+(row+1)).value;
	document.getElementsByClassName("percentage")[row].innerHTML = "%" + ((score/total_score)*100).toFixed(2);
	return ((score/total_score)*100);
}

function meanTotal(){
	var num_assi = document.getElementsByClassName("percentage");
	var num_row = document.getElementById("myTable").rows.length;
	var total_score = 0;
	for(var i = 0;i<num_assi.length;i++){
		total_score += showPercent(i);
	}
	document.getElementById("results").innerHTML = "%"+(total_score/num_assi.length).toFixed(2);
	document.getElementById("calculate_mean").style.bottom = "67px";
	document.getElementById("calculate_weight").style.bottom = "67px";
	document.getElementById("add_row").style.bottom = "67px";
	document.getElementById("delete_row").style.bottom = "67px";
}

function weightTotal(){
	var num_assi = document.getElementsByClassName("percentage");
	var weight = 0;
	var total_score = 0;
	for(var i = 0;i<num_assi.length;i++){
			total_score += showPercent(i) * Number(document.getElementById("weight"+(i+1)).value);
			weight += Number(document.getElementById("weight"+(i+1)).value);
	}
	document.getElementById("results").innerHTML = "%"+(total_score/weight).toFixed(2);
}

function addRow(){
	var table = document.getElementById("myTable");
	var num_row = document.getElementById("myTable").rows.length;
	var row = table.insertRow(num_row);
	var Name = row.insertCell(0);
	var ShortName = row.insertCell(1);
	Name.innerHTML = "Activity " + num_row;
	ShortName.innerHTML = "A" + num_row; //same as ShortName.innerHTML = "A ${num_row}"; looks cleaner

	// Creates an Input box in the data cell of a newly added row
	var Weight = row.insertCell(2);
  	var myWeight;
    myWeight = document.createElement('input');
    myWeight.setAttribute('type','number');
    myWeight.setAttribute('id', 'weight'+(num_row));
	Weight.appendChild(myWeight);

    //////////////////////////////////////////////////////////////////
	var Grade = row.insertCell(3);
    var myGrade,text,myTotal;
    var myBr = document.createElement('br');
    var myBr2 = document.createElement('br');
    myGrade = document.createElement('input');
    myGrade.setAttribute('type','number');
    myGrade.setAttribute('id','scored'+(num_row));
    myGrade.setAttribute('onkeyup','showPercent('+ (num_row-1) + ')');
    myTotal = document.createElement('input');
    myTotal.setAttribute('type','number');
    myTotal.setAttribute('id','total'+(num_row));
    myTotal.setAttribute('onkeyup','showPercent('+ (num_row-1) + ')');
    text = document.createTextNode(' /');
    Grade.appendChild(myGrade);
	Grade.appendChild(text);
	Grade.appendChild(myBr);
	Grade.appendChild(myBr2);
	Grade.appendChild(myTotal);
    ////////////////////////////////////////////////////////////////

	var Percentage = row.insertCell(4);
	var para = document.createElement('p');
	para.setAttribute('class','percentage');
	Percentage.appendChild(para);

	/*Calculation for height extension when adding rows*/
	document.getElementById('calc').style.height = ((num_row*93.75)+225) + "px";

	if((num_row*93.75)+375 >= 750){
		document.getElementById('inner-white-box').style.height = ((num_row*93.75)+375) + "px";
	}
}

function deleteRow(){
	var rows = document.getElementById('myTable').rows.length;
	if (rows > 1){
		document.getElementById("myTable").deleteRow(-1);
		document.getElementById('calc').style.height = (((rows-2)*93.75)+225) + "px";
		if (((rows-2)*93.75)+375 >= 750){
			document.getElementById('inner-white-box').style.height = (((rows-2)*93.75)+375) + "px";
		}
	}
	else{
		alert("You have no more assignments to delete!")
	}
}