const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 5000
var app = express();

/////////////////////////////
const {Client} = require('pg') //is equivalent to const Client = require('pg').Client
const client = new Client({ // same as const client = new Client("postgres://postgres:justinandjasper@localhost/assign2")
  user:"postgres",
  password:"justinandjasper",
  host:"localhost",
  port:5432,
  database:"assign2"
})
//client.connect()
// .then(()=>console.log("connected successfuly"))
//.then(()=>client.query("insert into tokimon values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)", ['Yeti', 'Ed', 50, 50, 0, 80, 70, 0, 90, 0, 0, 0, 340]))
// .then(()=>client.query("select * from tokimon")) //returns a table
// .then(results=>console.table(results.rows))
//.catch(error=>console.log(error))
// .finally(()=>client.end())
/////////////////////////////

//connection for heroku database? Can find on data.heroku.com
const{Pool} = require('pg')
var pool = new Pool({
  user:"ordorobzhmnfvt",
  password:"92e37da6cb8ff08a223afc8edb7b7d69cfd28dc90aa23716556d8e27785be82f",
  host:"ec2-54-243-208-234.compute-1.amazonaws.com",
  port:5432,
  database:"d26vp9hbtkk13i"
})


// const{Pool} = require('pg')
// var pool = new Pool({
//   user:"postgres",
//   password:"justinandjasper",
//   host:"localhost",
//   port:5432,
//   database:"assign2"
// })



app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => { //grabs table from database
  tokiTable(req,res);
})

//Adding Tokimon
app.post('/add', (req, res) => { //this currently works, fails when you include client.connect() and client.end()
  var name = req.body.toki_name;
  var weight = req.body.weight;
  var height = req.body.height;
  var fly = parseInt(req.body.fly);
  var fight = parseInt(req.body.fight);
  var water = parseInt(req.body.water);
  var electric = parseInt(req.body.electric);
  var ice = parseInt(req.body.ice);
  var earth = parseInt(req.body.earth);
  var psychic = parseInt(req.body.psychic);
  var spirit = parseInt(req.body.spirit);
  var trainer = req.body.trainer;
  var total = fly+fight+water+electric+ice+earth+psychic+spirit;
  pool.query("insert into tokimon values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)", [name, trainer, weight, height, fly, fight, water, electric, ice, earth, psychic, spirit, total]);
  tokiTable(req,res);
});

//Deleting Tokimon
app.post('/delete', (req,res)=>{
  var Id = req.body.Id;
  pool.query(`delete from tokimon where Id=${Id}`,(error,result)=>{
    if(error)
      res.send(error)
  });
  tokiTable(req,res);
});

//Edit Attributes
app.post('/edit', (req,res)=>{
  var Id = req.body.Id; //pool.query('select id from tokimon') outputs a promised{pending} | req.body.id returns undefined
  var weight = req.body.weight;
  var height = req.body.height;
  var fly = parseInt(req.body.fly);
  var fight = parseInt(req.body.fight);
  var water = parseInt(req.body.water);
  var electric = parseInt(req.body.electric);
  var ice = parseInt(req.body.ice);
  var earth = parseInt(req.body.earth);
  var psychic = parseInt(req.body.psychic);
  var spirit = parseInt(req.body.spirit);
  var total = fly+fight+water+electric+ice+earth+psychic+spirit;
  pool.query(`update tokimon set weight = ${weight}, height = ${height}, fly = ${fly}, fight = ${fight}, water = ${water}, electric = ${electric}, ice = ${ice}, earth = ${earth}, psychic = ${psychic}, spirit = ${spirit}, total = ${total} WHERE id=${Id}`); //putting [] around name = value causes syntax error
  tokiTable(req,res);
});

app.get('/Tokidex',(req,res)=>{
  tokiTable(req,res);
})

function tokiTable(req,res){
  var getToki = `select * from tokimon`;
  pool.query(getToki, (error,result)=>{
    if(error)
      res.send(error)
    var results = {'rows':result.rows};
    res.render('pages/Tokidex',results);
  })
}

app.listen(PORT, () => console.log(`Listening on ${ PORT }`));